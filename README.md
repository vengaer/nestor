# Nestor

This is a hack that automatically navigates `https://onlinebokning.pastelldata.com` and reserves entrance to the gym. The 
automatization requires systemd and an active X session.  

## How to Use

Nestor uses either Chrome or Firefox and automates the login and booking process. By default, the systemd timer is configured to run just after 4am each day. To enable it, do the following  
  
- Copy `nestor.json` to `$HOME/.config/nestor.json`.  
- Copy both `nestor.timer` and `nestor.service` to `$HOME/.config/systemd/user` (create the directory if it doesn't exist).  
- Install (copy) `nestor.py` to `usr/local/bin`.  
- Edit the json file and make sure it contains a valid username and password, the desired facility and the desired times.   
- Enable the *timer* using `systemctl --user enable nestor.timer`.  
- Enjoy saving about 10 seconds of your day.  

## Configuration

Nestor is configured in `$HOME/.config/nestor.json`. Most options should be self-explanatory. The `timeout` option is the max amount of time Nestor should wait for an element to load on the website. Usually, this will happen much sooner than the timeout expires.  

The `days_in_advance` option tells Nestor which day too book. As an example, if `days_in_advance` is 3 and Nestor runs on a Monday, it will book a time on the following Thursday.  

Which time to book each day is configured in the `times` list. This is simply a list of times to be booked for each day of the week (starting Monday). If not wanting to book a time for a specific day, leave its corresponding time field blank (or configure the systemd timer). As an example, if `times` is `["04:00", "06:00", "05:00", "", "06:00", "", ""]`, Nestor will book entrance Monday 4am, Tuesday 6am, Wednesday 5am, no entrance Thursday and so on. Note that the list contains the times that are to be booked, meaning that the above, assuming `days_in_advance` is 3, would result in Nestor _placing_ the bookings Friday, Saturday, Sunday and Tuesday.

## Disclaimer

This is a quick (and pretty fragile) hack. If anyone was to use this, I *will not* be held responsible for missed bookings etc.  

Lastly, if using this with systemd as intended, make sure this is run as a user service by following the steps outlined above. *NEVER* run this as root.
